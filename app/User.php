<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use Notifiable, HasRoles;
	use \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getUserRoleNotAdministrator()
    {
        return User::whereStatus(1)
        		   ->with(['roles' => function ($query) {
            			$query->where('name', '!=', 'administrator');
					}])
        		   ->get();
    }

	public function artikel()
	{
		return $this->hasMany('App\Artikel');
	}
}
