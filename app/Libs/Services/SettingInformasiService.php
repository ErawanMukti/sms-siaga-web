<?php

namespace App\Libs\Services;

use App\SettingInformasi;
use App\Libs\Contracts\SettingInformasiContract;
use Illuminate\Database\Eloquent\Collection;

class SettingInformasiService implements SettingInformasiContract
{
    private $model;

    public function __construct(SettingInformasi $model)
    {
        $this->model = $model;
    }

    /**
     * [getAllInformasi description]
     */
    public function getAllInformasi() : Collection
    {
    	return $this->model->get();
    }

    /**
     * [getOnlyAplikasi description]
     * @return Collection [description]
     */
    public function getOnlyAplikasi() : ?SettingInformasi
    {
    	return $this->model->whereModul('info-aplikasi')->first();
    }

    /**
     * [getOnlyBengkel description]
     * @return Array [description]
     */
    public function getOnlyBengkel() : Array
    {
    	$data['jam-operasional'] = $this->model->whereModul('jam-operasional')->first();
    	$data['alamat'] = $this->model->whereModul('alamat')->first();
    	$data['no-telepon'] = $this->model->whereModul('no-telepon')->first();
    	$data['lat'] = $this->model->whereModul('lat')->first();
    	$data['lng'] = $this->model->whereModul('lng')->first();

    	return $data;
    }

    /**
     * [updateInformasi description]
     * @param  array  $data [description]
     */
    public function saveInformasi(array $data) : void
    {
    	$this->model->updateOrCreate(['modul' => $data['modul']], $data);
    }
}
