<?php

namespace App\Libs\Services;

use App\Banner;
use App\Libs\Contracts\BannerContract;
use Illuminate\Database\Eloquent\Collection;

class BannerService implements BannerContract
{
    private $model;

    public function __construct(Banner $model)
    {
        $this->model = $model;
    }

    /**
     * [createBanner description]
     * @param  array  $data [description]
     */
    public function createBanner(array $data) : void
    {
    	$this->model->create($data);
    }

    /**
     * [deleteBanner description]
     * @param  int    $id [description]
     */
    public function deleteBanner(int $id) : void
    {
    	$this->model->findOrFail($id)->update(['status' => 0]);
    }

    /**
     * [getAllBanner description]
     * @return Collection [description]
     */
    public function getAllBanner() : Collection
    {
    	return $this->model->whereStatus(1)->orderBy('created_at', 'desc')->get();
    }

    /**
     * [getOnlyHome description]
     * @return Collection [description]
     */
    public function getOnlyHome() : Collection
    {
    	return $this->model->whereStatus(1)->whereModul('home')->orderBy('created_at', 'desc')->get();
    }

    /**
     * [getOnlyArtikel description]
     * @return Collection [description]
     */
    public function getOnlyArtikel() : Collection
    {
    	return $this->model->whereStatus(1)->whereModul('artikel')->orderBy('created_at', 'desc')->get();
    }
}
