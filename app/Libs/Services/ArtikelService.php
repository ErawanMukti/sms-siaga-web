<?php

namespace App\Libs\Services;

use App\Artikel;
use App\Libs\Contracts\ArtikelContract;
use Illuminate\Database\Eloquent\Collection;

class ArtikelService implements ArtikelContract
{
    private $model;

    public function __construct(Artikel $model)
    {
        $this->model = $model;
    }

    /**
     * [createArtikel description]
     * @param  array  $data [description]
     */
    public function createArtikel(array $data) : void
    {
    	$this->model->create($data);
    }

    /**
     * [deleteArtikel description]
     * @param  int    $id [description]
     */
    public function deleteArtikel(int $id) : void
    {
    	$this->model->findOrFail($id)->update(['status' => 0]);
    }

    /**
     * [getAllArtikel description]
     * @return Collection [description]
     */
    public function getAllArtikel() : Collection
    {
    	return $this->model->whereStatus(1)->orderBy('created_at', 'desc')->get();
    }

    public function getArtikelById(int $id) : ?Artikel
    {
    	return $this->model->findOrFail($id);
    }

    /**
     * [updateArtikel description]
     * @param  array  $data [description]
     * @param  int    $id   [description]
     */
    public function updateArtikel(array $data, int $id) : void
    {
    	$this->model->findOrFail($id)->update($data);
    }
}
