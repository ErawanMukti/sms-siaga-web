<?php

namespace App\Libs\Services;

use App\User;
use App\Libs\Contracts\UserContract;
use Illuminate\Database\Eloquent\Collection;

class UserService implements UserContract
{
    private $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * [createUser description]
     * @param  array  $data [description]
     */
    public function createUser(array $data) : ?User
    {
    	return $this->model->create($data);
    }

    /**
     * [deleteUser description]
     * @param  int    $id [description]
     */
    public function deleteUser(int $id) : void
    {
    	$this->model->findOrFail($id)->update(['status' => 0]);
    }

    /**
     * [updateUser description]
     * @param  array  $data [description]
     * @param  int    $id   [description]
     */
    public function updateUser(array $data, int $id) : void
    {
    	$this->model->findOrFail($id)->update($data);
    }
}
