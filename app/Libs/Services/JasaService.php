<?php

namespace App\Libs\Services;

use App\Jasa;
use App\Libs\Contracts\JasaContract;
use Illuminate\Database\Eloquent\Collection;

class JasaService implements JasaContract
{
    private $model;

    public function __construct(Jasa $model)
    {
        $this->model = $model;
    }

    /**
     * [createJasa description]
     * @param  array  $data [description]
     */
    public function createJasa(array $data) : void
    {
    	$this->model->create($data);
    }

    /**
     * [deleteJasa description]
     * @param  int    $id [description]
     */
    public function deleteJasa(int $id) : void
    {
    	$this->model->findOrFail($id)->update(['status' => 0]);
    }

    /**
     * [getAllJasa description]
     * @return Collection [description]
     */
    public function getAllJasa() : Collection
    {
    	return $this->model->whereStatus(1)->get();
    }

    public function getJasaByKategori(String $kategori) : ?Collection
    {
    	return $this->model->where('id_kategori', $kategori)->get();
    }
    /**
     * [updateJasa description]
     * @param  array  $data [description]
     * @param  int    $id   [description]
     */
    public function updateJasa(array $data, int $id) : void
    {
    	$this->model->findOrFail($id)->update($data);
    }
}
