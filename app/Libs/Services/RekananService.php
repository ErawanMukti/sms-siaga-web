<?php

namespace App\Libs\Services;

use App\Rekanan;
use App\Libs\Contracts\RekananContract;
use Illuminate\Database\Eloquent\Collection;

class RekananService implements RekananContract
{
    private $model;

    public function __construct(Rekanan $model)
    {
        $this->model = $model;
    }

    /**
     * [getAllRekanan description]
     * @return Collection [description]
     */
    public function getAllRekanan() : Collection
    {
    	return $this->model->whereStatus(1)->get();
    }

    /**
     * [createRekanan description]
     * @param  array  $data [description]
     */
    public function createRekanan(array $data) : void
    {
    	$this->model->create($data);
    }

    /**
     * [deleteRekanan description]
     * @param  int    $id [description]
     */
    public function deleteRekanan(int $id) : void
    {
    	$this->model->findOrFail($id)->update(['status' => 0]);
    }

    public function searchRekanan(String $q) : ?Collection
    {
    	return $this->model
    				->where('nama_bengkel', 'like', '%'.$q.'%')
    				->orWhere('kota', 'like', '%'.$q.'%')
    				->get();
    }

    /**
     * [updateRekanan description]
     * @param  array  $data [description]
     * @param  int    $id   [description]
     */
    public function updateRekanan(array $data, int $id) : void
    {
    	$this->model->findOrFail($id)->update($data);
    }
}
