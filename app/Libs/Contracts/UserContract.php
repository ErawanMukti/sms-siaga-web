<?php

namespace App\Libs\Contracts;

interface UserContract
{
    public function createUser(array $data);

    public function deleteUser(int $id);

    public function updateUser(array $data, int $id);
}