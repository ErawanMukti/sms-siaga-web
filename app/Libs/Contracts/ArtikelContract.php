<?php

namespace App\Libs\Contracts;

interface ArtikelContract
{
    public function createArtikel(array $data);

    public function deleteArtikel(int $id);

    public function getAllArtikel();

    public function getArtikelById(int $id);

    public function updateArtikel(array $data, int $id);
}