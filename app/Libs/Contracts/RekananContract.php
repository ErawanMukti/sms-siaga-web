<?php

namespace App\Libs\Contracts;

interface RekananContract
{
    public function getAllRekanan();

    public function createRekanan(array $data);

    public function deleteRekanan(int $id);

    public function searchRekanan(String $q);

    public function updateRekanan(array $data, int $id);
}