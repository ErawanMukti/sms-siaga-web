<?php

namespace App\Libs\Contracts;

interface SettingInformasiContract
{
    public function getAllInformasi();

    public function getOnlyAplikasi();

    public function getOnlyBengkel();

    public function saveInformasi(array $data);
}