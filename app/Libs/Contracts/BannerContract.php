<?php

namespace App\Libs\Contracts;

interface BannerContract
{
    public function createBanner(array $data);

    public function deleteBanner(int $id);

    public function getAllBanner();

    public function getOnlyHome();

    public function getOnlyArtikel();
}