<?php

namespace App\Libs\Contracts;

interface JasaContract
{
    public function createJasa(array $data);

    public function deleteJasa(int $id);

    public function getAllJasa();

    public function getJasaByKategori(String $kategori);

    public function updateJasa(array $data, int $id);
}