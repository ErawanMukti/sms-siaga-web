<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Artikel extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
	
    protected $guarded = [];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
