<?php

namespace App\Http\Controllers;

use App\Http\Requests\JasaRequest;
use App\Libs\Services\JasaService;
use Illuminate\Http\Request;
use Redirect;

class JasaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(JasaService $service)
    {
        $data = $service->getAllJasa();
        $kategori = config('kategori.kategori');

        return view('jasa.index', [
        	'data' => $data,
        	'kategori' => $kategori
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Request\JasaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JasaRequest $request, JasaService $service)
    {
        $data = $request->except(['_token']);
        $service->createJasa($data);

        return Redirect::to('jasa')->withSuccess('Berhasil menambah jasa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JasaRequest $request, $id, JasaService $service)
    {
        $data = $request->except(['_token', '_method']);
        $service->updateJasa($data, $id);

        return Redirect::to('jasa')->withSuccess('Berhasil merubah jasa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, JasaService $service)
    {
        $service->deleteJasa($id);

        return Redirect::to('jasa')->withSuccess('Berhasil menghapus jasa');
    }
}
