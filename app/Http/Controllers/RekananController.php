<?php

namespace App\Http\Controllers;

use App\Libs\Services\RekananService;
use Illuminate\Http\Request;
use Redirect;

class RekananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RekananService $service)
    {
        $data = $service->getAllRekanan();

        return view('rekanan.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RekananService $service)
    {
        $data = $request->except(['_token']);
        $data['status'] = 1;

        $service->createRekanan($data);

        return Redirect::to('rekanan')->withSuccess('Berhasil menambah data rekanan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, RekananService $service)
    {
        $data = $request->except(['_token', '_method']);
        
        $service->updateRekanan($data, $id);

        return Redirect::to('rekanan')->withSuccess('Berhasil merubah data rekanan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, RekananService $service)
    {
        $service->deleteRekanan($id);

        return Redirect::to('rekanan')->withSuccess('Berhasil menghapus data rekanan');
    }
}
