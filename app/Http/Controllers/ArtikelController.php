<?php

namespace App\Http\Controllers;

use App\Libs\Services\ArtikelService;
use Illuminate\Http\Request;
use Redirect;

class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ArtikelService $service)
    {
        $data = $service->getAllArtikel();

        return view('artikel.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ArtikelService $service)
    {
        $data = $request->except(['_token']);
        if ( $request->has('photo') ) {
			$data['photo'] = $request->file('photo')->store('artikel');
        }
        $data['user_id'] = $request->user()->id;
        $data['status'] = 1;

        $service->createArtikel($data);

        return Redirect::to('artikel')->withSuccess('Berhasil menambah artikel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, ArtikelService $service)
    {
        $data = $service->getArtikelById($id);

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, ArtikelService $service)
    {
        $data = $request->except(['_method', '_token', 'photo']);
        if ( $request->has('photo') ) {
        	$data['photo'] = $request->file('photo')->store('artikel');
        }

        $service->updateArtikel($data, $id);

        return Redirect::to('artikel')->withSuccess('Berhasil merubah artikel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, ArtikelService $service)
    {
        $service->deleteArtikel($id);

        return Redirect::to('artikel')->withSuccess('Berhasil menghapus artikel');
    }
}
