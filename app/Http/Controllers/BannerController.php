<?php

namespace App\Http\Controllers;

use App\Libs\Services\BannerService;
use Illuminate\Http\Request;
use Redirect;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BannerService $service)
    {
        $data = $service->getAllBanner();

        return view('banner.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, BannerService $service)
    {
        $request->validate([
    		'modul' => 'required',
            'photo' => 'required|dimensions:width=1280,height=460|max:1000'
		]);

        $data = $request->only(['modul']);
		$data['photo'] = $request->file('photo')->store('banner');
		$data['status'] = 1;

        $service->createBanner($data);

        return Redirect::to('banner')->withSuccess('Berhasil menambah banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, BannerService $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, BannerService $service)
    {
        $service->deleteBanner($id);

        return Redirect::to('banner')->withSuccess('Berhasil menghapus banner');
    }
}
