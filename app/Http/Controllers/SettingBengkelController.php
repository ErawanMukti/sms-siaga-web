<?php

namespace App\Http\Controllers;

use App\Libs\Services\SettingInformasiService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect;

class SettingBengkelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SettingInformasiService $service)
    {
        $data = $service->getOnlyBengkel();

        return view('setting.bengkel', ['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SettingInformasiService $service)
    {
    	DB::transaction(function() use ($request, $service) {
	    	foreach ($request->content as $key => $value) {
	    		$data['modul'] = $key;
	    		$data['content'] = $value;

	    		$service->saveInformasi($data);
	    	}
	    });
    	return Redirect::back()->withSuccess('Berhasil menyimpan informasi bengkel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
