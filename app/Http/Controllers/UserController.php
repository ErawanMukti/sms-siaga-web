<?php

namespace App\Http\Controllers;

use App\User;
use App\Libs\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$user = User::getUserRoleNotAdministrator();
    	$user = collect($user->toArray());

        $data = $user->reject(function ($value, $key) {
            return count($value['roles']) == 0;
        });

        return view('user.index', [
        	'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UserService $service)
    {
        $request->validate([
    		'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
		]);

    	DB::transaction(function() use ($request, $service) {
    		$data = $request->except(['_token', 'password']);
    		$data['password'] = bcrypt($request->password);
    		$data['status'] = 1;

	        $user = $service->createUser($data);
	        $user->assignRole('admin');
	    });
        return Redirect::to('user')->withSuccess('Berhasil menambah data user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, UserService $service)
    {
        $request->validate([
    		'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
            'password' => 'confirmed',
		]);

        $data = $request->except(['_token', '_method', 'password']);
        if ( $request->has('password') && !empty($request->password) ) {
            $data['password'] = bcrypt($request->password);
        }

        $user = $service->updateUser($data, $id);
        return Redirect::to('user')->withSuccess('Berhasil merubah data user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, UserService $service)
    {
        $service->deleteUser($id);
        return Redirect::to('user')->withSuccess('Berhasil menghapus data user');
    }
}
