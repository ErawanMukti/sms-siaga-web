<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libs\Services\ArtikelService;

class ArtikelController extends Controller
{
    public function getArtikel(ArtikelService $service)
    {
        $artikel = $service->getAllArtikel();

    	$data = [];
    	foreach ($artikel as $key => $value) {
    		$data[] = $value;
    	}

    	return response()->json([
    		'status' => 'ok',
    		'data' => $data
    	]);
    }
}
