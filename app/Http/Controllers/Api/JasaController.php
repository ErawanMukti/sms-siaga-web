<?php

namespace App\Http\Controllers\Api;

use App\Libs\Services\JasaService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JasaController extends Controller
{
    public function getJasa(JasaService $service)
    {
    	$kategori = config('kategori.kategori');

    	$data = [];
    	foreach ($kategori as $key => $value) {
    		$jasa = $service->getJasaByKategori($key)->toArray();
    		$data[] = [
    			'kode' => $key,
    			'nama' => $value['label'],
    			'jasa' => $jasa
    		];
    	}
    	
    	return response()->json([
    		'status' => 'ok',
    		'data' => $data
    	]);
    }
}
