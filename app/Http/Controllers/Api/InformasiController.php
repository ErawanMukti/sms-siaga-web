<?php

namespace App\Http\Controllers\Api;

use App\Libs\Services\SettingInformasiService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InformasiController extends Controller
{
    public function getInformasi(SettingInformasiService $service)
    {
    	$informasi = $service->getAllInformasi();

    	$data = [];
    	foreach ($informasi as $key => $value) {
    		$data[$value->modul] = $value->content;
    	}

    	return response()->json([
    		'status' => 'ok',
    		'data' => $data
    	]);
    }
}
