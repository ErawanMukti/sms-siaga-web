<?php

namespace App\Http\Controllers\Api;

use App\Libs\Services\RekananService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RekananController extends Controller
{
    public function getRekanan(RekananService $service)
    {
    	$data = $service->getAllRekanan();

    	return response()->json([
    		'status' => 'ok',
    		'data' => $data
    	]);    	
    }

    public function searchRekanan(String $q, RekananService $service)
    {
    	$data = $service->searchRekanan($q);

    	return response()->json([
    		'status' => 'ok',
    		'data' => $data
    	]);    	
    }
}
