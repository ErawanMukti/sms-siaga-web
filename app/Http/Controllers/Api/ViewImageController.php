<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ViewImageController extends Controller
{
    public function banner(Request $request)
    {
        return response()->file(storage_path('app/banner/' . $request->img));
    }

    public function artikel(Request $request)
    {
        return response()->file(storage_path('app/artikel/' . $request->img));
    }
}
