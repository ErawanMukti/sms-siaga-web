<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libs\Services\BannerService;

class BannerController extends Controller
{
    public function getOnlyHome(BannerService $service)
    {
    	$banner = $service->getOnlyHome();
    	$data = $banner[0]->photo;

    	return response()->json([
    		'status' => 'ok',
    		'data' => $data
    	]);
    }

    public function getOnlyArtikel(BannerService $service)
    {
    	$banner = $service->getOnlyArtikel();
    	$data = $banner[0]->photo;

    	return response()->json([
    		'status' => 'ok',
    		'data' => $data
    	]);
    }
}
