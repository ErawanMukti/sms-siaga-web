<?php

return [
    'permissions' => [
        'administrator' => [
            'name' => 'administrator',
            'label' => 'Administrator',
        ],
        'menu_daftar_rekanan' => [
            'name' => 'menu_daftar_rekanan',
            'label' => 'Menu Daftar Rekanan',
        ],
        'menu_konten' => [
            'name' => 'menu_konten',
            'label' => 'Menu Konten',
        ],
        'menu_master' => [
            'name' => 'menu_master',
            'label' => 'Menu master',
        ],
        'menu_setting_informasi' => [
            'name' => 'menu_setting_informasi',
            'label' => 'Menu Setting Informasi',
        ],
    ],

    'roles' => [
        'administrator' => [
            'name' => 'administrator',
            'label' => 'Administrator',
            'permissions' => [
                'administrator'
            ],
        ],
        'admin' => [
            'name' => 'admin',
            'label' => 'Admin',
            'permissions'=> [
                'menu_daftar_rekanan',
                'menu_konten',
                'menu_master',
                'menu_setting_informasi'
            ],
        ],
    ],
];
