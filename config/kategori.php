<?php

return [
	'kategori' => [
		'k1' => [
			'kode' => 'k1',
			'label' => 'Air Conditioner (AC)'
		],
		'k2' => [
			'kode' => 'k2',
			'label' => 'Aki'
		],
		'k3' => [
			'kode' => 'k3',
			'label' => 'Diagnosa'
		],
		'k4' => [
			'kode' => 'Kaca, Wiper, Spon',
			'label' => 'Struktur Organisasi'
		],
		'k5' => [
			'kode' => 'k5',
			'label' => 'Mesin'
		],
		'k6' => [
			'kode' => 'k6',
			'label' => 'Oli & Fluida'
		],
		'k7' => [
			'kode' => 'k7',
			'label' => 'Rem'
		],
		'k8' => [
			'kode' => 'k8',
			'label' => 'Service Berkala'
		],
		'k9' => [
			'kode' => 'k9',
			'label' => 'Service Umum'
		],
		'k10' => [
			'kode' => 'k10',
			'label' => 'Sistem Bahan Bakar'
		],
		'k11' => [
			'kode' => 'k11',
			'label' => 'Suspensi & Steering'
		],
		'k12' => [
			'kode' => 'k12',
			'label' => 'Transmisi & Kopling'
		],
		'k13' => [
			'kode' => 'k13',
			'label' => 'Tune Up'
		],
		'k14' => [
			'kode' => 'k14',
			'label' => 'Wiper'
		],
	],
];
