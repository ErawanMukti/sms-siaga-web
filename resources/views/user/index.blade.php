@extends('layouts.gentellela')

@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 text-right">
  			<button class="btn btn-app"
  				data-toggle="modal"
  				data-target="#modal_add">
  				<i class="fa fa-plus"></i> Tambah
  			</button>
  		</div>
	</div>
	<div class="row">
  		<div class="col-md-12 col-sm-12 col-xs-12">
    		<div class="x_panel">
      			<div class="x_title">
        			<h2>Daftar User</h2>
        			<div class="clearfix"></div>
      			</div>
      			<div class="x_content">
        			<div class="dashboard-widget-content">
          				<div class="col-md-12 hidden-small">
	            			<table id="datatable-fixed-header" class="table table-striped table-bordered">
	                  			<thead>
	                				<tr>
	                  					<th>Nama</th>
	                  					<th>Email</th>
	                  					<th width="80px">Action</th>
	                				</tr>
	              				</thead>
	              				<tbody>
	              				@foreach($data as $key => $value)
	                				<tr>
	                  					<td>{{ $value['name'] }}</td>
	                  					<td>{{ $value['email'] }}</td>
	                  					<td>
	                  						<button class="btn btn-warning"
	  											data-toggle="modal"
	  											data-target="#modal_update"
	  											data-url="{{ route('user.update', $value['id']) }}"
	  											data-name="{{ $value['name'] }}"
	  											data-email="{{ $value['email'] }}"
	  											title="Ubah Data">
	  											<i class="fa fa-edit""></i>
	  										</button>
	  										<a
	  											href="{{ route('user.destroy', $value['id']) }}"
	  											class="btn btn-danger helper"
	  											title="Hapus Data"
	  											data-method="delete"
	  											data-id="{{ $value['id'] }}"
	  											data-token="{{ csrf_token() }}">
	  											<i class="fa fa-trash" ></i>
	  										</a>
	                  					</td>
	                				</tr>
	              				@endforeach
	              				</tbody>
	            			</table>
          				</div>
        			</div>
      			</div>
    		</div>
		</div>
	</div>
</div>
<!-- /page content -->

<div id="modal_add" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <form action="{{ route('user.store') }}" id="form_add" method="post" class="modal-content">
            {{ csrf_field() }}

            <div class="modal-header bg-primary-600">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data User</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3">Nama*</label>
                    <div class="col-sm-9">
                    	<input id="name_add" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Email*</label>
                    <div class="col-sm-9">
                    	<input id="email_add" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Password*</label>
                    <div class="col-sm-9">
                    	<input id="password_add" type="password" class="form-control" name="password" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Konfirmasi Password*</label>
                    <div class="col-sm-9">
                    	<input id="password_confirmation_add" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b><i class="fa fa-close"></i></b> Batal</button>
                <button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>
            </div>
        </form>
    </div>
</div>

<div id="modal_update" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <form id="form_update" method="post" class="modal-content">
            {{ csrf_field() }}

            <input type="hidden" name="_method" value="put">

            <div class="modal-header bg-primary-600">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ubah Data User</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3">Nama*</label>
                    <div class="col-sm-9">
                    	<input id="name_update" type="text" class="form-control" name="name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Email*</label>
                    <div class="col-sm-9">
                    	<input id="email_update" type="email" class="form-control" name="email" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Password</label>
                    <div class="col-sm-9">
                    	<input id="password_update" type="password" class="form-control" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Konfirmasi Password</label>
                    <div class="col-sm-9">
                    	<input id="password_confirmation_update" type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b><i class="fa fa-close"></i></b> Batal</button>
                <button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
@if( Session::has('success') )
	<script type="text/javascript">
		swal("Berhasil", "{{ Session::get('success') }}", "success");
	</script>
@endif
@if( $errors->any() )
	<script type="text/javascript">
		swal("Tidak dapat menyimpan data", "{{ Html::ul($errors->all()) }}", "error");
	</script>
@endif

<script>
  	$(document).ready(function(){
        $('#datatable-fixed-header').DataTable({
          	fixedHeader: true
        });
        $.ajax({
            url: "{{ asset('js/helper.js') }}", dataType: "script",
        });
  	});

    $('#modal_update').on('show.bs.modal', function(e){
    	$("#form_update").prop('action', $(e.relatedTarget).data('url'));
    	$("#name_update").val($(e.relatedTarget).data('name'));
    	$("#email_update").val($(e.relatedTarget).data('email'));
	});
</script>
@endsection