@extends('layouts.gentellela')

@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 text-right">
  			<button class="btn btn-app"
  				data-toggle="modal"
  				data-target="#modal_add">
  				<i class="fa fa-plus"></i> Tambah
  			</button>
  		</div>
	</div>
	<div class="row">
  		<div class="col-md-12 col-sm-12 col-xs-12">
    		<div class="x_panel">
      			<div class="x_title">
        			<h2>Daftar Rekanan</h2>
        			<div class="clearfix"></div>
      			</div>
      			<div class="x_content">
        			<div class="dashboard-widget-content">
          				<div class="col-md-12 hidden-small">
	            			<table id="datatable-fixed-header" class="table table-striped table-bordered">
	                  			<thead>
	                				<tr>
	                  					<th>Nama Pemilik</th>
	                  					<th>Nama Bengkel</th>
	                  					<th>Alamat</th>
	                  					<th>Kota</th>
	                  					<th>No. Telepon</th>
	                  					<th width="80px">Action</th>
	                				</tr>
	              				</thead>
	              				<tbody>
	              				@foreach($data as $key => $value)
	                				<tr>
	                  					<td>{{ $value->nama_pemilik }}</td>
	                  					<td>{{ $value->nama_bengkel }}</td>
	                  					<td>{{ $value->alamat }}</td>
	                  					<td>{{ $value->kota }}</td>
	                  					<td>{{ $value->no_telepon }}</td>
	                  					<td>
	                  						<button class="btn btn-warning"
	  											data-toggle="modal"
	  											data-target="#modal_update"
	  											data-url="{{ route('rekanan.update', $value->id) }}"
	  											data-pemilik="{{ $value->nama_pemilik }}"
	  											data-bengkel="{{ $value->nama_bengkel }}"
	  											data-alamat="{{ $value->alamat }}"
	  											data-kota="{{ $value->kota }}"
	  											data-tlp="{{ $value->no_telepon }}"
	  											title="Ubah Data">
	  											<i class="fa fa-edit""></i>
	  										</button>
	  										<a
	  											href="{{ route('rekanan.destroy', $value->id) }}"
	  											class="btn btn-danger helper"
	  											title="Hapus Data"
	  											data-method="delete"
	  											data-id="{{ $value->id }}"
	  											data-token="{{ csrf_token() }}">
	  											<i class="fa fa-trash" ></i>
	  										</a>
	                  					</td>
	                				</tr>
	              				@endforeach
	              				</tbody>
	            			</table>
          				</div>
        			</div>
      			</div>
    		</div>
		</div>
	</div>
</div>
<!-- /page content -->

<div id="modal_add" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <form action="{{ route('rekanan.store') }}" id="form_add" method="post" class="modal-content">
            {{ csrf_field() }}

            <div class="modal-header bg-primary-600">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data Rekanan</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3">Nama Pemilik*</label>
                    <div class="col-sm-9">
                    	<input id="nama_pemilik_add" type="text" class="form-control" name="nama_pemilik" value="{{ old('nama_pemilik') }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Nama Bengkel*</label>
                    <div class="col-sm-9">
                    	<input id="nama_bengkel_add" type="text" class="form-control" name="nama_bengkel" value="{{ old('nama_bengkel') }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Alamat*</label>
                    <div class="col-sm-9">
                    	<textarea id="alamat_add" class="form-control" name="alamat" required>{{ old('alamat') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Kota*</label>
                    <div class="col-sm-9">
                    	<input id="kota_add" type="text" class="form-control" name="kota" value="{{ old('kota') }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">No Telepon</label>
                    <div class="col-sm-9">
                    	<input id="no_telepon_add" type="text" class="form-control" name="no_telepon" value="{{ old('no_telepon') }}">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b><i class="fa fa-close"></i></b> Batal</button>
                <button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>
            </div>
        </form>
    </div>
</div>

<div id="modal_update" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <form id="form_update" method="post" class="modal-content">
            {{ csrf_field() }}

            <input type="hidden" name="_method" value="put">

            <div class="modal-header bg-primary-600">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ubah Data Rekanan</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3">Nama Pemilik*</label>
                    <div class="col-sm-9">
                    	<input id="nama_pemilik_update" type="text" class="form-control" name="nama_pemilik" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Nama Bengkel*</label>
                    <div class="col-sm-9">
                    	<input id="nama_bengkel_update" type="text" class="form-control" name="nama_bengkel" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Alamat*</label>
                    <div class="col-sm-9">
                    	<textarea id="alamat_update" class="form-control" name="alamat" required>{{ old('alamat') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Kota*</label>
                    <div class="col-sm-9">
                    	<input id="kota_update" type="text" class="form-control" name="kota" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">No Telepon</label>
                    <div class="col-sm-9">
                    	<input id="no_telepon_update" type="text" class="form-control" name="no_telepon">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b><i class="fa fa-close"></i></b> Batal</button>
                <button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
@if( Session::has('success') )
	<script type="text/javascript">
		swal("Berhasil", "{{ Session::get('success') }}", "success");
	</script>
@endif
@if( $errors->any() )
	<script type="text/javascript">
		swal("Tidak dapat menyimpan data", "{{ Html::ul($errors->all()) }}", "error");
	</script>
@endif

<script>
  	$(document).ready(function(){
        $('#datatable-fixed-header').DataTable({
          	fixedHeader: true
        });
        $.ajax({
            url: "{{ asset('js/helper.js') }}", dataType: "script",
        });
  	});

    $('#modal_update').on('show.bs.modal', function(e){
    	$("#form_update").prop('action', $(e.relatedTarget).data('url'));
    	$("#nama_pemilik_update").val($(e.relatedTarget).data('pemilik'));
    	$("#nama_bengkel_update").val($(e.relatedTarget).data('bengkel'));
    	$("#alamat_update").val($(e.relatedTarget).data('alamat'));
    	$("#kota_update").val($(e.relatedTarget).data('kota'));
    	$("#no_telepon_update").val($(e.relatedTarget).data('tlp'));
	});
</script>
@endsection