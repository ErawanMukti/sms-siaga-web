@extends('layouts.gentellela')

@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 text-right">
  			<button class="btn btn-app"
  				data-toggle="modal"
  				data-target="#modal_add">
  				<i class="fa fa-plus"></i> Tambah
  			</button>
  		</div>
	</div>
	<div class="row">
  		<div class="col-md-12 col-sm-12 col-xs-12">
    		<div class="x_panel">
      			<div class="x_title">
        			<h2>Daftar Jasa/Layanan</h2>
        			<div class="clearfix"></div>
      			</div>
      			<div class="x_content">
        			<div class="dashboard-widget-content">
          				<div class="col-md-12 hidden-small">
	            			<table id="datatable-fixed-header" class="table table-striped table-bordered">
	                  			<thead>
	                				<tr>
	                  					<th>Kategori</th>
	                  					<th>Nama</th>
	                  					<th>Deskripsi</th>
	                  					<th>Harga</th>
	                  					<th width="80px">Action</th>
	                				</tr>
	              				</thead>
	              				<tbody>
	              				@foreach($data as $key => $value)
	                				<tr>
	                  					<td>{{ config('kategori.kategori.'.$value->id_kategori.'.label') }}</td>
	                  					<td>{{ $value->nama }}</td>
	                  					<td>{{ $value->deskripsi }}</td>
	                  					<td>{{ $value->harga }}</td>
	                  					<td>
	                  						<button class="btn btn-warning"
	  											data-toggle="modal"
	  											data-target="#modal_update"
	  											data-url="{{ route('jasa.update', $value->id) }}"
	  											data-kategori="{{ $value->id_kategori }}"
	  											data-nama="{{ $value->nama }}"
	  											data-deskripsi="{{ $value->deskripsi }}"
	  											data-harga="{{ $value->harga }}"
	  											title="Ubah Data">
	  											<i class="fa fa-edit""></i>
	  										</button>
	  										<a
	  											href="{{ route('jasa.destroy', $value->id) }}"
	  											class="btn btn-danger helper"
	  											title="Hapus Data"
	  											data-method="delete"
	  											data-id="{{ $value->id }}"
	  											data-token="{{ csrf_token() }}">
	  											<i class="fa fa-trash" ></i>
	  										</a>
	                  					</td>
	                				</tr>
	              				@endforeach
	              				</tbody>
	            			</table>
          				</div>
        			</div>
      			</div>
    		</div>
		</div>
	</div>
</div>
<!-- /page content -->

<div id="modal_add" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <form action="{{ route('jasa.store') }}" id="form_add" method="post" class="modal-content">
            {{ csrf_field() }}

            <div class="modal-header bg-primary-600">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data Jasa/Layanan</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3">Kategori*</label>
                    <div class="col-sm-9">
                    	<select id="kategori_add" class="form-control" name="id_kategori">
                		@foreach($kategori as $key => $value)
                			<option value="{{ $key }}">{{ $value['label'] }}</option>
                		@endforeach
                    	</select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Nama*</label>
                    <div class="col-sm-9">
                    	<input id="nama_add" type="text" class="form-control" name="nama" value="{{ old('nama') }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Deskripsi*</label>
                    <div class="col-sm-9">
                    	<textarea id="deskripsi_add" class="form-control" name="deskripsi" rows="5" required>{{ old('deskripsi') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Harga*</label>
                    <div class="col-sm-9">
                    	<input id="harga_add" type="text" class="form-control" name="harga" value="{{ old('harga') }}" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b><i class="fa fa-close"></i></b> Batal</button>
                <button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>
            </div>
        </form>
    </div>
</div>

<div id="modal_update" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <form id="form_update" method="post" class="modal-content">
            {{ csrf_field() }}

            <input type="hidden" name="_method" value="put">

            <div class="modal-header bg-primary-600">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ubah Data Jasa/Layanan</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3">Kategori*</label>
                    <div class="col-sm-9">
                    	<select id="kategori_update" class="form-control" name="id_kategori">
                		@foreach($kategori as $key => $value)
                			<option value="{{ $key }}">{{ $value['label'] }}</option>
                		@endforeach
                    	</select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Nama*</label>
                    <div class="col-sm-9">
                    	<input id="nama_update" type="text" class="form-control" name="nama" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Deskripsi*</label>
                    <div class="col-sm-9">
                    	<textarea id="deskripsi_update" class="form-control" name="deskripsi" rows="5" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Harga*</label>
                    <div class="col-sm-9">
                    	<input id="harga_update" type="text" class="form-control" name="harga" value="{{ old('harga') }}" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b><i class="fa fa-close"></i></b> Batal</button>
                <button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
@if( Session::has('success') )
	<script type="text/javascript">
		swal("Berhasil", "{{ Session::get('success') }}", "success");
	</script>
@endif
@if( $errors->any() )
	<script type="text/javascript">
		swal("Tidak dapat menyimpan data", "{{ Html::ul($errors->all()) }}", "error");
	</script>
@endif

<script>
  	$(document).ready(function(){
        $('#datatable-fixed-header').DataTable({
          	fixedHeader: true
        });
        $.ajax({
            url: "{{ asset('js/helper.js') }}", dataType: "script",
        });
  	});

    $('#modal_update').on('show.bs.modal', function(e){
    	$("#form_update").prop('action', $(e.relatedTarget).data('url'));
    	$("#kategori_update").val($(e.relatedTarget).data('kategori'));
    	$("#nama_update").val($(e.relatedTarget).data('nama'));
    	$("#deskripsi_update").val($(e.relatedTarget).data('deskripsi'));
    	$("#harga_update").val($(e.relatedTarget).data('harga'));
	});
</script>
@endsection