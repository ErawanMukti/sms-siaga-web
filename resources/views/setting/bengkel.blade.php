@extends('layouts.gentellela')

@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
  		<div class="col-md-12 col-sm-12 col-xs-12">
    		<div class="x_panel">
      			<div class="x_title">
        			<h2>Informasi Bengkel</h2>
        			<div class="clearfix"></div>
      			</div>
      			<div class="x_content">
        			<div class="dashboard-widget-content">
          				<div class="col-md-12 hidden-small">
					        <form action="{{ route('setting-bengkel.store') }}" id="form_add" method="post" class="form-horizontal">
					            {{ csrf_field() }}

				                <div class="form-group">
				                    <label class="control-label col-sm-2">Jam Operasional</label>
				                    <div class="col-sm-10">
				                    	<textarea id="jam-operasional" class="form-control" name="content[jam-operasional]" rows="5">{{ $data['jam-operasional']->content ?? '-' }}</textarea>
				                    </div>
				                </div>

				                <div class="form-group">
				                    <label class="control-label col-sm-2">Alamat</label>
				                    <div class="col-sm-10">
				                    	<textarea id="alamat" class="form-control" name="content[alamat]" rows="5">{{ $data['alamat']->content ?? '-' }}</textarea>
				                    </div>
				                </div>

				                <div class="form-group">
				                    <label class="control-label col-sm-2">No. Telepon</label>
				                    <div class="col-sm-10">
				                    	<input type="text" id="no-telepon" class="form-control" name="content[no-telepon]" value="{{ $data['no-telepon']->content ?? '-' }}">

				                    </div>
				                </div>

				                <div class="form-group">
				                    <label class="control-label col-sm-2">Latitude</label>
				                    <div class="col-sm-10">
				                    	<input type="text" id="lat" class="form-control" name="content[lat]" value="{{ $data['lat']->content ?? '-' }}">
				                    </div>
				                </div>

				                <div class="form-group">
				                    <label class="control-label col-sm-2">Longitude</label>
				                    <div class="col-sm-10">
				                    	<input type="text" id="lng" class="form-control" name="content[lng]" value="{{ $data['lng']->content ?? '-' }}">
				                    </div>
				                </div>

		                      	<div class="ln_solid"></div>

		                      	<div class="form-group">
		                        	<div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-2">
					                	<button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>	
		                        	</div>
		                      	</div>
					        </form>
          				</div>
        			</div>
      			</div>
    		</div>
		</div>
	</div>
</div>
<!-- /page content -->
@endsection

@section('js')
@if( Session::has('success') )
	<script type="text/javascript">
		swal("Berhasil", "{{ Session::get('success') }}", "success");
	</script>
@endif
@if( $errors->any() )
	<script type="text/javascript">
		swal("Tidak dapat menyimpan data", "{{ Html::ul($errors->all()) }}", "error");
	</script>
@endif
@endsection