@extends('layouts.gentellela')

@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
  		<div class="col-md-12 col-sm-12 col-xs-12">
    		<div class="x_panel">
      			<div class="x_title">
        			<h2>Informasi Aplikasi</h2>
        			<div class="clearfix"></div>
      			</div>
      			<div class="x_content">
        			<div class="dashboard-widget-content">
          				<div class="col-md-12 hidden-small">
					        <form action="{{ route('setting-aplikasi.store') }}" id="form_add" method="post" class="form-horizontal">
					            {{ csrf_field() }}

				                <div class="form-group">
				                    <label class="control-label col-sm-2">Konten</label>
				                    <div class="col-sm-10">
				                    	<textarea id="content" class="form-control" name="content" rows="10">{{ $data->content ?? '-' }}</textarea>
				                    </div>
				                </div>

		                      	<div class="ln_solid"></div>

		                      	<div class="form-group">
		                        	<div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-2">
					                	<button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>	
		                        	</div>
		                      	</div>
					        </form>
          				</div>
        			</div>
      			</div>
    		</div>
		</div>
	</div>
</div>
<!-- /page content -->
@endsection

@section('js')
@if( Session::has('success') )
	<script type="text/javascript">
		swal("Berhasil", "{{ Session::get('success') }}", "success");
	</script>
@endif
@if( $errors->any() )
	<script type="text/javascript">
		swal("Tidak dapat menyimpan data", "{{ Html::ul($errors->all()) }}", "error");
	</script>
@endif
@endsection