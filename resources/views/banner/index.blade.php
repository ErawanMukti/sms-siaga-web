@extends('layouts.gentellela')

@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 text-right">
  			<button class="btn btn-app"
  				data-toggle="modal"
  				data-target="#modal_add">
  				<i class="fa fa-plus"></i> Tambah
  			</button>
  		</div>
	</div>
	<div class="row">
  		<div class="col-md-12 col-sm-12 col-xs-12">
    		<div class="x_panel">
      			<div class="x_title">
        			<h2>Daftar Banner</h2>
        			<div class="clearfix"></div>
      			</div>
      			<div class="x_content">
        			<div class="dashboard-widget-content">
          				<div class="col-md-12 hidden-small">
	            			<table id="datatable-fixed-header" class="table table-striped table-bordered">
	                  			<thead>
	                				<tr>
	                  					<th>Modul</th>
	                  					<th width="400px">Foto</th>
	                  					<th>Tanggal</th>
	                  					<th width="80px">Action</th>
	                				</tr>
	              				</thead>
	              				<tbody>
	              				@foreach($data as $key => $value)
	                				<tr>
	                  					<td>{{ $value->modul }}</td>
	                  					<td>
	                  						<img src="{{ route('view-image') . '?file='.$value->photo }}" width="400px" height="auto" />
	                  					</td>
	                  					<td>{{ $value->created_at }}</td>
	                  					<td>
	  										<a
	  											href="{{ route('banner.destroy', $value->id) }}"
	  											class="btn btn-danger helper"
	  											title="Hapus Data"
	  											data-method="delete"
	  											data-id="{{ $value->id }}"
	  											data-token="{{ csrf_token() }}">
	  											<i class="fa fa-trash" ></i>
	  										</a>
	                  					</td>
	                				</tr>
	              				@endforeach
	              				</tbody>
	            			</table>
          				</div>
        			</div>
      			</div>
    		</div>
		</div>
	</div>
</div>
<!-- /page content -->

<div id="modal_add" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <form action="{{ route('banner.store') }}" id="form_add" method="post" class="modal-content" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="modal-header bg-primary-600">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Banner</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3">Modul*</label>
                    <div class="col-sm-9">
                    	<select id="modul_add" type="text" class="form-control" name="modul">
                    		<option value="home">Homepage</option>
                    		<option value="artikel">Artikel</option>
                    	</select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Photo (1280 x 460)*</label>
                    <div class="col-sm-9">
                    	<input id="photo_add" type="file" class="form-control" name="photo" accept="image/*" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b><i class="fa fa-close"></i></b> Batal</button>
                <button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
@if( Session::has('success') )
	<script type="text/javascript">
		swal("Berhasil", "{{ Session::get('success') }}", "success");
	</script>
@endif
@if( $errors->any() )
	<script type="text/javascript">
		swal("Tidak dapat menyimpan data", "{{ Html::ul($errors->all()) }}", "error");
	</script>
@endif

<script>
  	$(document).ready(function(){
        $('#datatable-fixed-header').DataTable({
          	fixedHeader: true
        });
        $.ajax({
            url: "{{ asset('js/helper.js') }}", dataType: "script",
        });
  	});
</script>
@endsection