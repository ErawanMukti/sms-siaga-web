@extends('layouts.gentellela')

@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 text-right">
  			<button class="btn btn-app"
  				data-toggle="modal"
  				data-target="#modal_add">
  				<i class="fa fa-plus"></i> Tambah
  			</button>
  		</div>
	</div>
	<div class="row">
  		<div class="col-md-12 col-sm-12 col-xs-12">
    		<div class="x_panel">
      			<div class="x_title">
        			<h2>Daftar Post/Artikel</h2>
        			<div class="clearfix"></div>
      			</div>
      			<div class="x_content">
        			<div class="dashboard-widget-content">
          				<div class="col-md-12 hidden-small">
	            			<table id="datatable-fixed-header" class="table table-striped table-bordered">
	                  			<thead>
	                				<tr>
	                  					<th>Judul</th>
	                  					<th>Ringkasan</th>
	                  					<th>Penulis</th>
	                  					<th width="80px">Action</th>
	                				</tr>
	              				</thead>
	              				<tbody>
	              				@foreach($data as $key => $value)
	                				<tr>
	                  					<td>{{ $value->judul }}</td>
	                  					<td>{{ $value->ringkasan }}</td>
	                  					<td>{{ $value->user->name }}</td>
	                  					<td>
	                  						<button class="btn btn-warning"
	  											data-toggle="modal"
	  											data-target="#modal_update"
	  											data-url="{{ route('artikel.update', $value->id) }}"
	  											data-show-url="{{ route('artikel.show', $value->id) }}"
	  											title="Ubah Data">
	  											<i class="fa fa-edit""></i>
	  										</button>
	  										<a
	  											href="{{ route('artikel.destroy', $value->id) }}"
	  											class="btn btn-danger helper"
	  											title="Hapus Data"
	  											data-method="delete"
	  											data-id="{{ $value->id }}"
	  											data-token="{{ csrf_token() }}">
	  											<i class="fa fa-trash" ></i>
	  										</a>
	                  					</td>
	                				</tr>
	              				@endforeach
	              				</tbody>
	            			</table>
          				</div>
        			</div>
      			</div>
    		</div>
		</div>
	</div>
</div>
<!-- /page content -->

<div id="modal_add" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <form action="{{ route('artikel.store') }}" id="form_add" method="post" class="modal-content" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="modal-header bg-primary-600">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data Post/Artikel</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3">Judul*</label>
                    <div class="col-sm-9">
                    	<input id="judul_add" type="text" class="form-control" name="judul" value="{{ old('judul') }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Ringkasan*</label>
                    <div class="col-sm-9">
                    	<textarea id="ringkasan_add" class="form-control" name="ringkasan" rows="3" maxlength="200" required>{{ old('ringkasan') }}</textarea>
                    	<p>(Max: 200 Kata)</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Konten*</label>
                    <div class="col-sm-9">
                    	<textarea id="isi_add" class="form-control" name="isi" rows="5" required>{{ old('isi') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Photo</label>
                    <div class="col-sm-9">
                    	<input id="photo_add" type="file" class="form-control" name="photo" accept="image/*">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b><i class="fa fa-close"></i></b> Batal</button>
                <button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>
            </div>
        </form>
    </div>
</div>

<div id="modal_update" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <form id="form_update" method="post" class="modal-content" enctype="multipart/form-data">
            {{ csrf_field() }}

            <input type="hidden" name="_method" value="put">

            <div class="modal-header bg-primary-600">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ubah Data Post/Artikel</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3">Judul*</label>
                    <div class="col-sm-9">
                    	<input id="judul_update" type="text" class="form-control" name="judul" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Ringkasan*</label>
                    <div class="col-sm-9">
                    	<textarea id="ringkasan_update" class="form-control" name="ringkasan" rows="3" maxlength="200" required></textarea>
                    	<p>(Max: 200 Kata)</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Konten*</label>
                    <div class="col-sm-9">
                    	<textarea id="isi_update" class="form-control" name="isi" rows="5" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Photo</label>
                    <div class="col-sm-9">
                    	<input id="photo_update" type="file" class="form-control" name="photo" accept="image/*">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b><i class="fa fa-close"></i></b> Batal</button>
                <button type="submit" class="btn btn-success"><b><i class="fa fa-save"></i></b> Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
@if( Session::has('success') )
	<script type="text/javascript">
		swal("Berhasil", "{{ Session::get('success') }}", "success");
	</script>
@endif
@if( $errors->any() )
	<script type="text/javascript">
		swal("Tidak dapat menyimpan data", "{{ Html::ul($errors->all()) }}", "error");
	</script>
@endif

<script>
  	$(document).ready(function(){
        $('#datatable-fixed-header').DataTable({
          	fixedHeader: true
        });
        $.ajax({
            url: "{{ asset('js/helper.js') }}", dataType: "script",
        });
  	});

    $('#modal_update').on('show.bs.modal', function(e){
    	var url = $(e.relatedTarget).data('url');
    	var show_url = $(e.relatedTarget).data('show-url');

    	$("#form_update").prop('action', url);
    	
    	$.ajax({
            type : 'get',
            url: show_url,
            success: function(data){
            	$("#judul_update").val(data['judul']);
            	$("#ringkasan_update").val(data['ringkasan']);
            	$("#isi_update").val(data['isi']);
            }
        });
	});
</script>
@endsection