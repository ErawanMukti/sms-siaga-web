<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('artikels', 'Api\ArtikelController@getArtikel');
Route::get('artikel_banner', 'Api\BannerController@getOnlyArtikel');
Route::get('home_banner', 'Api\BannerController@getOnlyHome');
Route::get('informasis', 'Api\InformasiController@getInformasi');
Route::get('jasas', 'Api\JasaController@getJasa');
Route::get('rekanans', 'Api\RekananController@getRekanan');
Route::get('rekanan/{q}', 'Api\RekananController@searchRekanan');
Route::get('preview_banner/{img}', 'Api\ViewImageController@banner');
Route::get('preview_artikel/{img}', 'Api\ViewImageController@artikel');