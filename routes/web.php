<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/view-image', 'PreviewImageController@index')->name('view-image');

Auth::routes();

Route::group(['middleware' => 'auth'], function ()
{
	Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['middleware' => 'roles', 'roles' => 'administrator'], function ()
    {
		Route::get('admin', 'Admin\AdminController@index');
		Route::resource('admin/roles', 'Admin\RolesController');
		Route::resource('admin/permissions', 'Admin\PermissionsController');
		Route::resource('admin/users', 'Admin\UsersController');
		Route::resource('admin/pages', 'Admin\PagesController');
		Route::resource('admin/activitylogs', 'Admin\ActivityLogsController')->only([
		    'index', 'show', 'destroy'
		]);
		Route::resource('admin/settings', 'Admin\SettingsController');
		Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
		Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
		Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
	});

    Route::group(['middleware' => 'roles', 'roles' => 'admin'], function ()
    {
		Route::resource('artikel', 'ArtikelController');
    	Route::resource('banner', 'BannerController');
    	Route::resource('jasa', 'JasaController');
    	Route::resource('rekanan', 'RekananController');
    	Route::resource('setting-aplikasi', 'SettingAplikasiController');
    	Route::resource('setting-bengkel', 'SettingBengkelController');
    	Route::resource('user', 'UserController');
    });
});