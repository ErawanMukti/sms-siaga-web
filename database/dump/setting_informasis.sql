INSERT INTO `setting_informasis` (`id`, `modul`, `content`, `created_at`, `updated_at`) VALUES
(1, 'jam-operasional', 'Senin-Kamis: 07:30-16:30\r\nJumat: Libur\r\nSabtu-Minggu: 07:30-16:30', '2018-10-07 10:01:53', '2018-10-07 10:01:53'),
(2, 'alamat', 'Jl. Jeruk III B Kav. 253\r\nWage, Taman, Kabupaten Sidoarjo\r\nJawa Timur 61257', '2018-10-07 10:01:53', '2018-10-07 10:01:53'),
(3, 'no-telepon', '+628112023379', '2018-10-07 10:01:53', '2018-10-07 10:01:53'),
(4, 'lat', '112.707897', '2018-10-07 10:01:53', '2018-10-07 10:01:53'),
(5, 'lng', '-7.374671', '2018-10-07 10:01:53', '2018-10-07 10:01:53'),
(6, 'info-aplikasi', 'Bengkel Mobil Siaga adalah aplikasi yang dibangun oleh Setiawan Motor Service Sidoarjo dalam rangka memudahkan kami dalam memberikan informasi dan pelayanan kepada customer.\r\nDengan berbekal pengalaman dan profesionalitas teknisi kami siap membantu apapun permasalahan mobil anda, dengan rekanan yang tersebar di seluruh jawa timur dimanapun anda berada kami akan selalu siap membantu.\r\nMobil Terawat, Perjalanan Tenang Karena Kami Selalu Bersama Anda.', '2018-10-07 10:03:08', '2018-10-07 10:03:08');