<?php

use App\Libs\Traits\RefreshRolePermission;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    use RefreshRolePermission;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->refreshRolePermission();

        $user = factory(App\User::class)->create([
            'email' => 'airexhck@gmail.com',
            'name' => 'Administrator',
            'password' => bcrypt('hacker')
        ]);
        $user->assignRole(config('rolepermission.roles.administrator.name'));

        $user = factory(App\User::class)->create([
            'email' => 'admin@smsmotor.com',
            'name' => 'Admin',
            'password' => bcrypt('admin')
        ]);
        $user->assignRole(config('rolepermission.roles.admin.name'));

        Eloquent::unguard();
        $path = 'database/dump/setting_informasis.sql';
        DB::unprepared(file_get_contents($path));

        $path = 'database/dump/rekanans.sql';
        DB::unprepared(file_get_contents($path));
    }
}
